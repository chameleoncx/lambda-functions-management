const AWS = require('aws-sdk');
const sqs = new AWS.SQS();
const s3 = new AWS.S3();
const wkhtmltopdf = require('./src/helpers/wkhtmltopdf');
const fs = require('fs-extra');
const hbs = require('handlebars');
const path = require('path');

exports.handler = function (event) {
    createPdf(event.Records[0].body);
};

const compile = async function (pdfData, templateName) {
    const filePath = path.join(process.cwd(), 'templates', `${templateName}.hbs`);
    const html = await fs.readFile(filePath, 'utf-8');
    return hbs.compile(html)(pdfData);
}

const createPdf = async function (data) {
    const templateData = JSON.parse(data);
    const content = await compile(templateData, 'camie-wf');
    wkhtmltopdf(content, { pageSize: 'letter' }, function (err, stream) {
        if (err) {
            console.log('error in pdf generation', err);
        } else {
            uploadPdf(stream, templateData);
        }

    });
}
const uploadPdf = function (pdfdata, mainData) {
    var param = {
        Bucket: process.env.bucketName,
        Key: process.env.fileName,
        Body: pdfdata,
        ContentType: "application/pdf"
    }
    s3.upload(param, async function (err, response) {
        if (err) {
            console.log('error', err);
        } else {
            console.log('success', response);
            const event = {
                "source": process.env.sourceName,
                "to": mainData.to,
                "from": mainData.from,
                "subject": mainData.subject,
                "cc": '',
                "bcc": '',
                "body": "",
                "attachURL": response.Location,
                "bucket": response.Bucket,
                "file": response.Key
            }
            console.log(event);
            const mainQ = process.env.MAIN_Q;
            const paramData = {
                MessageAttributes: {
                    "Title": {
                        DataType: "String",
                        StringValue: "The Whistler"
                    },
                    "Author": {
                        DataType: "String",
                        StringValue: "John Grisham"
                    },
                    "WeeksOn": {
                        DataType: "Number",
                        StringValue: "6"
                    }
                },
                MessageBody: JSON.stringify(event),
                QueueUrl: mainQ
            };
            sqs.sendMessage(paramData, function (err, data) {
                if (err) {
                    console.log(`Error while pushing message to queue ${err}`);
                } else {
                    console.log(`Message Sent successfully ${data.MessageId}`);
                }
            });

        }
    });
}