FROM adoptopenjdk/openjdk11:alpine-jre
RUN addgroup -S chameleon && adduser -S chameleon -G chameleon
USER chameleon:chameleon
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=${ENV}","app.jar"]