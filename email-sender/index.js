const AWS = require('aws-sdk');
const nodemailer = require("nodemailer");
const sqs = new AWS.SQS();
const ses = new AWS.SES();
const s3 = new AWS.S3();
const WEB_SOURCE = 'web';
const WF_SOURCE = 'wf';

exports.handler = (event) => {
    sendEmail(event.Records[0].body);
};

const sendEmail = (data) => {
    const emailData = JSON.parse(data);
    if ((emailData.source).toLowerCase() == WEB_SOURCE) {
        const mailOptions = {
            from: emailData.from,
            subject: emailData.subject,
            html: `${emailData.body}`,
            to: emailData.to,
            bcc: emailData.bcc
        };

        const transporter = nodemailer.createTransport({
            SES: ses
        });

        transporter.sendMail(mailOptions, function (err, info) {
            if (err) {
                console.log(`Error in sending email ${err}`);
            } else {
                console.log('Email sent successfully');
            }
        });
    } else if ((emailData.source).toLowerCase() == WF_SOURCE) {
        getS3File(emailData.bucket, emailData.file)
            .then(function (fileData) {
                const mailOptions = {
                    from: emailData.from,
                    subject: emailData.subject,
                    html: `${emailData.body}`,
                    to: emailData.to,
                    bcc: emailData.bcc,
                    attachments: [
                        {
                            filename: emailData.file,
                            content: fileData.Body
                        }
                    ]
                };

                console.log('Creating SES transporter');
                const transporter = nodemailer.createTransport({
                    SES: ses
                });

                transporter.sendMail(mailOptions, function (err, info) {
                    if (err) {
                        console.log(`Error sending email ${err}`);
                    } else {
                        console.log('Email sent successfully');
                    }
                });
            })
            .catch(function (error) {
                console.log(`Error getting attachment from S3 ${error}`);
            });
    }
};

function getS3File(bucket, key) {
    return new Promise(function (resolve, reject) {
        s3.getObject(
            {
                Bucket: bucket,
                Key: key
            },
            function (err, data) {
                if (err) {
                    return reject(err);
                } else {
                    return resolve(data);
                }
            }
        );
    });
}

